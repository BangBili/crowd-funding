<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => 'users', 'middleware' => 'jwt.verify'], function () {
    Route::get('/', 'UserController@index');
    Route::post('/', 'UserController@store');
    Route::get('/profile', 'UserController@show');
    
});


Route::group(['prefix' => 'auth'], function () {    
    Route::post('/login', 'AuthController@login');
    Route::post('/set-password/{user}', 'AuthController@setPassword');
    Route::post('/register', 'AuthController@registration');
    Route::get('/validate-otp-code/{otp}', 'AuthController@validateOtp');
    Route::get('/regenerate-otp-code/{email}', 'AuthController@regenerateOtp');
    
});

