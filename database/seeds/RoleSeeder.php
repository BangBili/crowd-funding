<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            [
                'id'    => 1,
                'name'  => 'Admin',
                'role'  => 'admin',
            ],
            [
                'id'    => 2,
                'name'  => 'User',
                'role'  => 'user',
            ],
        ];

        foreach ($role as $key) {
            Role::updateOrCreate([
                'id' => $key['id']
            ],[
                'id'    => $key['id'],
                'name'  => $key['name'],
                'role'  => $key['role'],
            ]);
        }
    }
}
