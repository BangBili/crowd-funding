<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtpCode extends Model
{
    protected $fillable = [
        'user_id','otp_code','expired_in'
    ];

    protected $casts = [
        'expired_in' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
