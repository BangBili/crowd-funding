<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegenerateOtpCode extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $user, $otp;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->otp = $user->otp_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
        ->view('send_email_user_regenerate_otp')
        ->with([
            'name' => $this->user->name,
            'otp' => $this->otp->otp_code,
        ]);
    }
}
