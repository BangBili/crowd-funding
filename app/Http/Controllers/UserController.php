<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = User::with(['otp_code'])->get();

        return response()->json($model);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = User::create($request->validated());
        $this->createOtpCode($user);

        $msg = [
            'success' => true,
            'message' => 'Data User berhasil di tambah',
            'user' => $user->load(['role','otp_code'])
        ];
 
        return response()->json($msg);
    }

    public function show()
    {
        $user = auth()->user();
        return response()->json($user);
    }

    public function update (UserStoreRequest $request, User $user)
    {
        $user->update($request->validated());
        $file = $request->input('file');
        if(!empty($file)) {
            $directory = 'user/'.$user->id;
            $changedName = time().random_int(100,999).$file->getClientOriginalName();

            $file->storeAs($directory, $changedName);


            $user->photo_path = Storage::url('app/'.$directory.'/'.$changedName);
            $user->save();
        }

        $msg = [
            'success' => true,
            'message' => 'Data User berhasil di tambah',
            'user' => $user
        ];
 
        return response()->json($msg);
    }
 
}
