<?php

namespace App\Http\Controllers;

use App\Events\UserRegenerateOtpCodeEvent;
use App\Events\UserRegisteredEvent;
use App\Http\Requests\RegisterFormRequest;
use App\Mail\UserRegisteredEmail;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    // login
    public function login(){
        $credentials = request()->only(['name', 'password']);

        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
         }
        if(auth()->user()->email_verified_at) {
            return $this->createNewToken($token);
        } else {
            return response()->json(
                [
                    'code' => 01,
                    'message' => 'akun belum diverifikasi'
                ]
            );
        }
    }

    // create token
    protected function createNewToken($token){
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => JwtAuth::factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    // registrasi
    public function registration (RegisterFormRequest $request)
    {
        $user = User::create($request->validated());
        $this->createOtpCode($user);

        event(new UserRegisteredEvent($user));

        $msg = [
            'code' => 00,
            'message' => 'Berhasil register, silahkan cek email anda untuk verifikasi kode otp',
            'user' => $user->load(['otp_code'])
        ];
 
        return response()->json($msg);
    }

    // store kode otp
    public function createOtpCode(User $user)
    {
        $model= new OtpCode();
        $arrayOtp = [
            'user_id' => $user->id,
            'otp_code' => $this->generateRandomString(),
            'expired_in' => Carbon::now()->addMinute(5) 
        ];
        $model = $model->create($arrayOtp);

    }
    // buat random kode otp
    function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // validate otp
    public function validateOtp($otp)
    {

        $user = User::whereHas('otp_code', function($query) use($otp){
            $query->where('otp_code',$otp);
        })
        ->get()
        ->first();
        // return response()->json($user);

        if($user) {
            $otp_code = OtpCode::where('user_id',$user->id)->get()->first();
            // return response()->json($otp_code);
            if($otp_code->expired_in <= Carbon::now()) {

                $user->email_verified_at = Carbon::now();
                $user->save();

                $msg = [
                    'code' => 00,
                    'message' => 'Berhasil verifikasi email, silahkan Login',
                    'user' => $user->load(['otp_code'])
                ];
                return response()->json($msg);
            } else {
                $msg = [
                    'code' => 01,
                    'message' => 'Gagal verifikasi, nomor hangus, silahkan buat ulang otp anda',
                ];
                return response()->json($msg);
            }
        } else {
            $msg = [
                'code' => 01,
                'message' => 'Gagal, pokoknya kode otp salah, silahkan buat ulang kode otp',
            ];
            return response()->json($msg);
        }
        
    }

    public function regenerateOtp($email)
    {
        $model= OtpCode::whereHas('user', function($query) use($email){
            $query->where('email',$email);
        }) ->get()->first();
        // return response()->json($);
        if($model) {
            
            $model->otp_code = $this->generateRandomString();
            $model->expired_in = Carbon::now()->addMinute(5);
            
            $model->save();
            $user = $model->user;
            
            $msg = [
                'code' => 00,
                'message' => 'Berhasil validasi ulang',
                'otp' => $user
            ];
            event(new UserRegenerateOtpCodeEvent($user));
            // event(new UserRegisteredEvent($user));
            return response()->json($msg);
        } else {
            $msg = [
                'code' => 01,
                'message' => 'Gagal, email tidak terbaca',
            ];
            return response()->json($msg);
        }

    }

    public function setPassword(User $user, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        if($user->email_verified_at != null) {
            $user->update([
                'password' => Hash::make($request->input('password'))
            ]);

            return response()->json([
                'code' => 00,
                'message' => 'password berhasil dibuat',
                'user' => $user
            ], 200);
        } else {
            return response()->json([
                'code' => 01,
                'message' => 'password gagal dibuat, akun belum di validasi',
                'user' => $user
            ], 200);
        }



    }

}
