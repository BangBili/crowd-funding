<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class EmailVerifiedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! $request->route('user')) {
            $msg = [
                'code' => 405,
                'message' => 'Tidak bisa masuk, user tidak terdaftar',
                'data' => $request
            ];
     
            return response()->json($msg);
        }
        if(! $request->route('user')->email_verified_at) {
            $msg = [
                'code' => 405,
                'message' => 'Tidak bisa masuk, email user tidak terverifikasi',
                'data' => $request->route('user')
            ];
     
            return response()->json($msg);
        }
        else {
            return $next($request);
        }

    }
}
