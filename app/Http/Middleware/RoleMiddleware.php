<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(! $request->route('user')) {
            $msg = [
                'code' => 405,
                'message' => 'Tidak bisa masuk, user tidak terdaftar',
                'data' => $request
            ];
     
            return response()->json($msg);
        }
        if($request->route('user')->role_id != 1) {
            $msg = [
                'code' => 405,
                'message' => 'Tidak bisa masuk, bukan admin',
                'data' => $request->route('user')->role_id
            ];
     
            return response()->json($msg);
        }
        else {
            return $next($request);
        }
    }
}
