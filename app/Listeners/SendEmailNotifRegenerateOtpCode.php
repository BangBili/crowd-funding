<?php

namespace App\Listeners;


use App\Events\UserRegenerateOtpCodeEvent;
use App\Mail\UserRegenerateOtpCode;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotifRegenerateOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserRegenerateOtpCodeEvent $event)
    {
        Mail::to($event->user)->send(new UserRegenerateOtpCode($event->user));
    }
}
